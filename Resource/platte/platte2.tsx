<?xml version="1.0" encoding="UTF-8"?>
<tileset name="platte2_130x75" tilewidth="128" tileheight="128" tilecount="64" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0001.png"/>
 </tile>
 <tile id="1">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0005.png"/>
 </tile>
 <tile id="2">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0009.png"/>
 </tile>
 <tile id="3">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0013.png"/>
 </tile>
 <tile id="4">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0017.png"/>
 </tile>
 <tile id="5">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0021.png"/>
 </tile>
 <tile id="6">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0025.png"/>
 </tile>
 <tile id="7">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0029.png"/>
 </tile>
 <tile id="8">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0033.png"/>
 </tile>
 <tile id="9">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0037.png"/>
 </tile>
 <tile id="10">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0041.png"/>
 </tile>
 <tile id="11">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0045.png"/>
 </tile>
 <tile id="12">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0049.png"/>
 </tile>
 <tile id="13">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0053.png"/>
 </tile>
 <tile id="14">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0057.png"/>
 </tile>
 <tile id="15">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0061.png"/>
 </tile>
 <tile id="16">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0065.png"/>
 </tile>
 <tile id="17">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0069.png"/>
 </tile>
 <tile id="18">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0073.png"/>
 </tile>
 <tile id="19">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0077.png"/>
 </tile>
 <tile id="20">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0081.png"/>
 </tile>
 <tile id="21">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0085.png"/>
 </tile>
 <tile id="22">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0089.png"/>
 </tile>
 <tile id="23">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0093.png"/>
 </tile>
 <tile id="24">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0097.png"/>
 </tile>
 <tile id="25">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0101.png"/>
 </tile>
 <tile id="26">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0105.png"/>
 </tile>
 <tile id="27">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0109.png"/>
 </tile>
 <tile id="28">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0113.png"/>
 </tile>
 <tile id="29">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0117.png"/>
 </tile>
 <tile id="30">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0121.png"/>
 </tile>
 <tile id="31">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0125.png"/>
 </tile>
 <tile id="32">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0129.png"/>
 </tile>
 <tile id="33">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0133.png"/>
 </tile>
 <tile id="34">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0137.png"/>
 </tile>
 <tile id="35">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0141.png"/>
 </tile>
 <tile id="36">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0145.png"/>
 </tile>
 <tile id="37">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0149.png"/>
 </tile>
 <tile id="38">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0153.png"/>
 </tile>
 <tile id="39">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0157.png"/>
 </tile>
 <tile id="40">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0161.png"/>
 </tile>
 <tile id="41">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0165.png"/>
 </tile>
 <tile id="42">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0169.png"/>
 </tile>
 <tile id="43">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0173.png"/>
 </tile>
 <tile id="44">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0177.png"/>
 </tile>
 <tile id="45">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0181.png"/>
 </tile>
 <tile id="46">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0185.png"/>
 </tile>
 <tile id="47">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0189.png"/>
 </tile>
 <tile id="48">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0193.png"/>
 </tile>
 <tile id="49">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0197.png"/>
 </tile>
 <tile id="50">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0201.png"/>
 </tile>
 <tile id="51">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0205.png"/>
 </tile>
 <tile id="52">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0209.png"/>
 </tile>
 <tile id="53">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0213.png"/>
 </tile>
 <tile id="54">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0217.png"/>
 </tile>
 <tile id="55">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0221.png"/>
 </tile>
 <tile id="56">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0225.png"/>
 </tile>
 <tile id="57">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0229.png"/>
 </tile>
 <tile id="58">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0233.png"/>
 </tile>
 <tile id="59">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0237.png"/>
 </tile>
 <tile id="60">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0241.png"/>
 </tile>
 <tile id="61">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0245.png"/>
 </tile>
 <tile id="62">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0249.png"/>
 </tile>
 <tile id="63">
  <image width="128" height="128" source="../图块-Kenney/2/h01_s128_iso_0253.png"/>
 </tile>
</tileset>

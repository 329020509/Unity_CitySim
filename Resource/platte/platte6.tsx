<?xml version="1.0" encoding="UTF-8"?>
<tileset name="platte6" tilewidth="133" tileheight="130" tilecount="229" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="132" height="112" source="../图块-Kenney/6/PNG/Details/crystals_1.png"/>
 </tile>
 <tile id="1">
  <image width="132" height="121" source="../图块-Kenney/6/PNG/Details/crystals_2.png"/>
 </tile>
 <tile id="2">
  <image width="133" height="127" source="../图块-Kenney/6/PNG/Details/crystals_3.png"/>
 </tile>
 <tile id="3">
  <image width="132" height="114" source="../图块-Kenney/6/PNG/Details/crystals_4.png"/>
 </tile>
 <tile id="4">
  <image width="133" height="99" source="../图块-Kenney/6/PNG/Details/rocks_1.png"/>
 </tile>
 <tile id="5">
  <image width="133" height="99" source="../图块-Kenney/6/PNG/Details/rocks_2.png"/>
 </tile>
 <tile id="6">
  <image width="133" height="102" source="../图块-Kenney/6/PNG/Details/rocks_3.png"/>
 </tile>
 <tile id="7">
  <image width="133" height="102" source="../图块-Kenney/6/PNG/Details/rocks_4.png"/>
 </tile>
 <tile id="8">
  <image width="133" height="99" source="../图块-Kenney/6/PNG/Details/rocks_5.png"/>
 </tile>
 <tile id="9">
  <image width="133" height="99" source="../图块-Kenney/6/PNG/Details/rocks_6.png"/>
 </tile>
 <tile id="10">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Details/rocks_7.png"/>
 </tile>
 <tile id="11">
  <image width="133" height="99" source="../图块-Kenney/6/PNG/Details/rocks_8.png"/>
 </tile>
 <tile id="12">
  <image width="133" height="111" source="../图块-Kenney/6/PNG/Details/trees_1.png"/>
 </tile>
 <tile id="13">
  <image width="133" height="121" source="../图块-Kenney/6/PNG/Details/trees_2.png"/>
 </tile>
 <tile id="14">
  <image width="133" height="113" source="../图块-Kenney/6/PNG/Details/trees_3.png"/>
 </tile>
 <tile id="15">
  <image width="133" height="127" source="../图块-Kenney/6/PNG/Details/trees_4.png"/>
 </tile>
 <tile id="16">
  <image width="132" height="126" source="../图块-Kenney/6/PNG/Details/trees_5.png"/>
 </tile>
 <tile id="17">
  <image width="133" height="124" source="../图块-Kenney/6/PNG/Details/trees_6.png"/>
 </tile>
 <tile id="18">
  <image width="133" height="121" source="../图块-Kenney/6/PNG/Details/trees_7.png"/>
 </tile>
 <tile id="19">
  <image width="133" height="118" source="../图块-Kenney/6/PNG/Details/trees_8.png"/>
 </tile>
 <tile id="20">
  <image width="133" height="116" source="../图块-Kenney/6/PNG/Details/trees_9.png"/>
 </tile>
 <tile id="21">
  <image width="132" height="130" source="../图块-Kenney/6/PNG/Details/trees_10.png"/>
 </tile>
 <tile id="22">
  <image width="133" height="118" source="../图块-Kenney/6/PNG/Details/trees_11.png"/>
 </tile>
 <tile id="23">
  <image width="133" height="123" source="../图块-Kenney/6/PNG/Details/trees_12.png"/>
 </tile>
 <tile id="24">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_00.png"/>
 </tile>
 <tile id="25">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_01.png"/>
 </tile>
 <tile id="26">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_02.png"/>
 </tile>
 <tile id="27">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_03.png"/>
 </tile>
 <tile id="28">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_04.png"/>
 </tile>
 <tile id="29">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_05.png"/>
 </tile>
 <tile id="30">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_06.png"/>
 </tile>
 <tile id="31">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_07.png"/>
 </tile>
 <tile id="32">
  <image width="132" height="115" source="../图块-Kenney/6/PNG/Landscape/landscape_08.png"/>
 </tile>
 <tile id="33">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_09.png"/>
 </tile>
 <tile id="34">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_10.png"/>
 </tile>
 <tile id="35">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_11.png"/>
 </tile>
 <tile id="36">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_12.png"/>
 </tile>
 <tile id="37">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_13.png"/>
 </tile>
 <tile id="38">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_14.png"/>
 </tile>
 <tile id="39">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_15.png"/>
 </tile>
 <tile id="40">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_16.png"/>
 </tile>
 <tile id="41">
  <image width="132" height="83" source="../图块-Kenney/6/PNG/Landscape/landscape_17.png"/>
 </tile>
 <tile id="42">
  <image width="132" height="115" source="../图块-Kenney/6/PNG/Landscape/landscape_18.png"/>
 </tile>
 <tile id="43">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_19.png"/>
 </tile>
 <tile id="44">
  <image width="132" height="115" source="../图块-Kenney/6/PNG/Landscape/landscape_20.png"/>
 </tile>
 <tile id="45">
  <image width="132" height="83" source="../图块-Kenney/6/PNG/Landscape/landscape_21.png"/>
 </tile>
 <tile id="46">
  <image width="132" height="115" source="../图块-Kenney/6/PNG/Landscape/landscape_22.png"/>
 </tile>
 <tile id="47">
  <image width="132" height="115" source="../图块-Kenney/6/PNG/Landscape/landscape_23.png"/>
 </tile>
 <tile id="48">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_24.png"/>
 </tile>
 <tile id="49">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_25.png"/>
 </tile>
 <tile id="50">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_26.png"/>
 </tile>
 <tile id="51">
  <image width="132" height="115" source="../图块-Kenney/6/PNG/Landscape/landscape_27.png"/>
 </tile>
 <tile id="52">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_28.png"/>
 </tile>
 <tile id="53">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_29.png"/>
 </tile>
 <tile id="54">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_30.png"/>
 </tile>
 <tile id="55">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_31.png"/>
 </tile>
 <tile id="56">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_32.png"/>
 </tile>
 <tile id="57">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_33.png"/>
 </tile>
 <tile id="58">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_34.png"/>
 </tile>
 <tile id="59">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_35.png"/>
 </tile>
 <tile id="60">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_36.png"/>
 </tile>
 <tile id="61">
  <image width="133" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_37.png"/>
 </tile>
 <tile id="62">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_38.png"/>
 </tile>
 <tile id="63">
  <image width="132" height="99" source="../图块-Kenney/6/PNG/Landscape/landscape_39.png"/>
 </tile>
 <tile id="64">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_00.png"/>
 </tile>
 <tile id="65">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (brown)/tower_01.png"/>
 </tile>
 <tile id="66">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (brown)/tower_02.png"/>
 </tile>
 <tile id="67">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (brown)/tower_03.png"/>
 </tile>
 <tile id="68">
  <image width="78" height="79" source="../图块-Kenney/6/PNG/Towers (brown)/tower_04.png"/>
 </tile>
 <tile id="69">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_05.png"/>
 </tile>
 <tile id="70">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (brown)/tower_06.png"/>
 </tile>
 <tile id="71">
  <image width="83" height="73" source="../图块-Kenney/6/PNG/Towers (brown)/tower_07.png"/>
 </tile>
 <tile id="72">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (brown)/tower_08.png"/>
 </tile>
 <tile id="73">
  <image width="87" height="74" source="../图块-Kenney/6/PNG/Towers (brown)/tower_09.png"/>
 </tile>
 <tile id="74">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (brown)/tower_10.png"/>
 </tile>
 <tile id="75">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_11.png"/>
 </tile>
 <tile id="76">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (brown)/tower_12.png"/>
 </tile>
 <tile id="77">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (brown)/tower_13.png"/>
 </tile>
 <tile id="78">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_14.png"/>
 </tile>
 <tile id="79">
  <image width="87" height="74" source="../图块-Kenney/6/PNG/Towers (brown)/tower_15.png"/>
 </tile>
 <tile id="80">
  <image width="89" height="74" source="../图块-Kenney/6/PNG/Towers (brown)/tower_16.png"/>
 </tile>
 <tile id="81">
  <image width="78" height="79" source="../图块-Kenney/6/PNG/Towers (brown)/tower_17.png"/>
 </tile>
 <tile id="82">
  <image width="78" height="79" source="../图块-Kenney/6/PNG/Towers (brown)/tower_18.png"/>
 </tile>
 <tile id="83">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (brown)/tower_19.png"/>
 </tile>
 <tile id="84">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (brown)/tower_20.png"/>
 </tile>
 <tile id="85">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_21.png"/>
 </tile>
 <tile id="86">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_22.png"/>
 </tile>
 <tile id="87">
  <image width="89" height="74" source="../图块-Kenney/6/PNG/Towers (brown)/tower_23.png"/>
 </tile>
 <tile id="88">
  <image width="89" height="98" source="../图块-Kenney/6/PNG/Towers (brown)/tower_24.png"/>
 </tile>
 <tile id="89">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (brown)/tower_25.png"/>
 </tile>
 <tile id="90">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (brown)/tower_26.png"/>
 </tile>
 <tile id="91">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_27.png"/>
 </tile>
 <tile id="92">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_28.png"/>
 </tile>
 <tile id="93">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_29.png"/>
 </tile>
 <tile id="94">
  <image width="89" height="103" source="../图块-Kenney/6/PNG/Towers (brown)/tower_30.png"/>
 </tile>
 <tile id="95">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (brown)/tower_31.png"/>
 </tile>
 <tile id="96">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_32.png"/>
 </tile>
 <tile id="97">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_33.png"/>
 </tile>
 <tile id="98">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_34.png"/>
 </tile>
 <tile id="99">
  <image width="93" height="98" source="../图块-Kenney/6/PNG/Towers (brown)/tower_35.png"/>
 </tile>
 <tile id="100">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (brown)/tower_36.png"/>
 </tile>
 <tile id="101">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_37.png"/>
 </tile>
 <tile id="102">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_38.png"/>
 </tile>
 <tile id="103">
  <image width="93" height="98" source="../图块-Kenney/6/PNG/Towers (brown)/tower_39.png"/>
 </tile>
 <tile id="104">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (brown)/tower_40.png"/>
 </tile>
 <tile id="105">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_41.png"/>
 </tile>
 <tile id="106">
  <image width="89" height="100" source="../图块-Kenney/6/PNG/Towers (brown)/tower_42.png"/>
 </tile>
 <tile id="107">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (brown)/tower_43.png"/>
 </tile>
 <tile id="108">
  <image width="89" height="100" source="../图块-Kenney/6/PNG/Towers (brown)/tower_44.png"/>
 </tile>
 <tile id="109">
  <image width="79" height="71" source="../图块-Kenney/6/PNG/Towers (brown)/tower_45.png"/>
 </tile>
 <tile id="110">
  <image width="79" height="71" source="../图块-Kenney/6/PNG/Towers (brown)/tower_46.png"/>
 </tile>
 <tile id="111">
  <image width="95" height="76" source="../图块-Kenney/6/PNG/Towers (brown)/tower_47.png"/>
 </tile>
 <tile id="112">
  <image width="95" height="76" source="../图块-Kenney/6/PNG/Towers (brown)/tower_48.png"/>
 </tile>
 <tile id="113">
  <image width="90" height="74" source="../图块-Kenney/6/PNG/Towers (brown)/tower_49.png"/>
 </tile>
 <tile id="114">
  <image width="90" height="74" source="../图块-Kenney/6/PNG/Towers (brown)/tower_50.png"/>
 </tile>
 <tile id="115">
  <image width="103" height="78" source="../图块-Kenney/6/PNG/Towers (brown)/tower_51.png"/>
 </tile>
 <tile id="116">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (brown)/tower_52.png"/>
 </tile>
 <tile id="117">
  <image width="103" height="78" source="../图块-Kenney/6/PNG/Towers (brown)/tower_53.png"/>
 </tile>
 <tile id="118">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (brown)/tower_54.png"/>
 </tile>
 <tile id="119">
  <image width="89" height="100" source="../图块-Kenney/6/PNG/Towers (grey)/tower_00.png"/>
 </tile>
 <tile id="120">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_01.png"/>
 </tile>
 <tile id="121">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_02.png"/>
 </tile>
 <tile id="122">
  <image width="95" height="76" source="../图块-Kenney/6/PNG/Towers (grey)/tower_03.png"/>
 </tile>
 <tile id="123">
  <image width="95" height="76" source="../图块-Kenney/6/PNG/Towers (grey)/tower_04.png"/>
 </tile>
 <tile id="124">
  <image width="90" height="75" source="../图块-Kenney/6/PNG/Towers (grey)/tower_05.png"/>
 </tile>
 <tile id="125">
  <image width="90" height="75" source="../图块-Kenney/6/PNG/Towers (grey)/tower_06.png"/>
 </tile>
 <tile id="126">
  <image width="103" height="78" source="../图块-Kenney/6/PNG/Towers (grey)/tower_07.png"/>
 </tile>
 <tile id="127">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_08.png"/>
 </tile>
 <tile id="128">
  <image width="103" height="78" source="../图块-Kenney/6/PNG/Towers (grey)/tower_09.png"/>
 </tile>
 <tile id="129">
  <image width="92" height="75" source="../图块-Kenney/6/PNG/Towers (grey)/tower_10.png"/>
 </tile>
 <tile id="130">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_11.png"/>
 </tile>
 <tile id="131">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (grey)/tower_12.png"/>
 </tile>
 <tile id="132">
  <image width="92" height="75" source="../图块-Kenney/6/PNG/Towers (grey)/tower_13.png"/>
 </tile>
 <tile id="133">
  <image width="92" height="75" source="../图块-Kenney/6/PNG/Towers (grey)/tower_14.png"/>
 </tile>
 <tile id="134">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (grey)/tower_15.png"/>
 </tile>
 <tile id="135">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_16.png"/>
 </tile>
 <tile id="136">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (grey)/tower_17.png"/>
 </tile>
 <tile id="137">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (grey)/tower_18.png"/>
 </tile>
 <tile id="138">
  <image width="92" height="75" source="../图块-Kenney/6/PNG/Towers (grey)/tower_19.png"/>
 </tile>
 <tile id="139">
  <image width="86" height="74" source="../图块-Kenney/6/PNG/Towers (grey)/tower_20.png"/>
 </tile>
 <tile id="140">
  <image width="87" height="82" source="../图块-Kenney/6/PNG/Towers (grey)/tower_21.png"/>
 </tile>
 <tile id="141">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_22.png"/>
 </tile>
 <tile id="142">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (grey)/tower_23.png"/>
 </tile>
 <tile id="143">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (grey)/tower_24.png"/>
 </tile>
 <tile id="144">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_25.png"/>
 </tile>
 <tile id="145">
  <image width="86" height="74" source="../图块-Kenney/6/PNG/Towers (grey)/tower_26.png"/>
 </tile>
 <tile id="146">
  <image width="89" height="74" source="../图块-Kenney/6/PNG/Towers (grey)/tower_27.png"/>
 </tile>
 <tile id="147">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (grey)/tower_28.png"/>
 </tile>
 <tile id="148">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (grey)/tower_29.png"/>
 </tile>
 <tile id="149">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (grey)/tower_30.png"/>
 </tile>
 <tile id="150">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (grey)/tower_31.png"/>
 </tile>
 <tile id="151">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_32.png"/>
 </tile>
 <tile id="152">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_33.png"/>
 </tile>
 <tile id="153">
  <image width="89" height="74" source="../图块-Kenney/6/PNG/Towers (grey)/tower_34.png"/>
 </tile>
 <tile id="154">
  <image width="89" height="98" source="../图块-Kenney/6/PNG/Towers (grey)/tower_35.png"/>
 </tile>
 <tile id="155">
  <image width="87" height="82" source="../图块-Kenney/6/PNG/Towers (grey)/tower_36.png"/>
 </tile>
 <tile id="156">
  <image width="83" height="73" source="../图块-Kenney/6/PNG/Towers (grey)/tower_37.png"/>
 </tile>
 <tile id="157">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_38.png"/>
 </tile>
 <tile id="158">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_39.png"/>
 </tile>
 <tile id="159">
  <image width="78" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_40.png"/>
 </tile>
 <tile id="160">
  <image width="89" height="103" source="../图块-Kenney/6/PNG/Towers (grey)/tower_41.png"/>
 </tile>
 <tile id="161">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (grey)/tower_42.png"/>
 </tile>
 <tile id="162">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_43.png"/>
 </tile>
 <tile id="163">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_44.png"/>
 </tile>
 <tile id="164">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_45.png"/>
 </tile>
 <tile id="165">
  <image width="93" height="98" source="../图块-Kenney/6/PNG/Towers (grey)/tower_46.png"/>
 </tile>
 <tile id="166">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (grey)/tower_47.png"/>
 </tile>
 <tile id="167">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_48.png"/>
 </tile>
 <tile id="168">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_49.png"/>
 </tile>
 <tile id="169">
  <image width="93" height="98" source="../图块-Kenney/6/PNG/Towers (grey)/tower_50.png"/>
 </tile>
 <tile id="170">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (grey)/tower_51.png"/>
 </tile>
 <tile id="171">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (grey)/tower_52.png"/>
 </tile>
 <tile id="172">
  <image width="89" height="100" source="../图块-Kenney/6/PNG/Towers (grey)/tower_53.png"/>
 </tile>
 <tile id="173">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (grey)/tower_54.png"/>
 </tile>
 <tile id="174">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (red)/tower_00.png"/>
 </tile>
 <tile id="175">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (red)/tower_01.png"/>
 </tile>
 <tile id="176">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (red)/tower_02.png"/>
 </tile>
 <tile id="177">
  <image width="78" height="79" source="../图块-Kenney/6/PNG/Towers (red)/tower_03.png"/>
 </tile>
 <tile id="178">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_04.png"/>
 </tile>
 <tile id="179">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (red)/tower_05.png"/>
 </tile>
 <tile id="180">
  <image width="83" height="73" source="../图块-Kenney/6/PNG/Towers (red)/tower_06.png"/>
 </tile>
 <tile id="181">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (red)/tower_07.png"/>
 </tile>
 <tile id="182">
  <image width="87" height="74" source="../图块-Kenney/6/PNG/Towers (red)/tower_08.png"/>
 </tile>
 <tile id="183">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (red)/tower_09.png"/>
 </tile>
 <tile id="184">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_10.png"/>
 </tile>
 <tile id="185">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (red)/tower_11.png"/>
 </tile>
 <tile id="186">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (red)/tower_12.png"/>
 </tile>
 <tile id="187">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_13.png"/>
 </tile>
 <tile id="188">
  <image width="87" height="74" source="../图块-Kenney/6/PNG/Towers (red)/tower_14.png"/>
 </tile>
 <tile id="189">
  <image width="89" height="74" source="../图块-Kenney/6/PNG/Towers (red)/tower_15.png"/>
 </tile>
 <tile id="190">
  <image width="78" height="79" source="../图块-Kenney/6/PNG/Towers (red)/tower_16.png"/>
 </tile>
 <tile id="191">
  <image width="78" height="79" source="../图块-Kenney/6/PNG/Towers (red)/tower_17.png"/>
 </tile>
 <tile id="192">
  <image width="79" height="70" source="../图块-Kenney/6/PNG/Towers (red)/tower_18.png"/>
 </tile>
 <tile id="193">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (red)/tower_19.png"/>
 </tile>
 <tile id="194">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_20.png"/>
 </tile>
 <tile id="195">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_21.png"/>
 </tile>
 <tile id="196">
  <image width="89" height="74" source="../图块-Kenney/6/PNG/Towers (red)/tower_22.png"/>
 </tile>
 <tile id="197">
  <image width="89" height="98" source="../图块-Kenney/6/PNG/Towers (red)/tower_23.png"/>
 </tile>
 <tile id="198">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (red)/tower_24.png"/>
 </tile>
 <tile id="199">
  <image width="84" height="73" source="../图块-Kenney/6/PNG/Towers (red)/tower_25.png"/>
 </tile>
 <tile id="200">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_26.png"/>
 </tile>
 <tile id="201">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_27.png"/>
 </tile>
 <tile id="202">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_28.png"/>
 </tile>
 <tile id="203">
  <image width="89" height="103" source="../图块-Kenney/6/PNG/Towers (red)/tower_29.png"/>
 </tile>
 <tile id="204">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (red)/tower_30.png"/>
 </tile>
 <tile id="205">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_31.png"/>
 </tile>
 <tile id="206">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_32.png"/>
 </tile>
 <tile id="207">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_33.png"/>
 </tile>
 <tile id="208">
  <image width="93" height="98" source="../图块-Kenney/6/PNG/Towers (red)/tower_34.png"/>
 </tile>
 <tile id="209">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (red)/tower_35.png"/>
 </tile>
 <tile id="210">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_36.png"/>
 </tile>
 <tile id="211">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_37.png"/>
 </tile>
 <tile id="212">
  <image width="93" height="98" source="../图块-Kenney/6/PNG/Towers (red)/tower_38.png"/>
 </tile>
 <tile id="213">
  <image width="88" height="82" source="../图块-Kenney/6/PNG/Towers (red)/tower_39.png"/>
 </tile>
 <tile id="214">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_40.png"/>
 </tile>
 <tile id="215">
  <image width="89" height="100" source="../图块-Kenney/6/PNG/Towers (red)/tower_41.png"/>
 </tile>
 <tile id="216">
  <image width="79" height="79" source="../图块-Kenney/6/PNG/Towers (red)/tower_42.png"/>
 </tile>
 <tile id="217">
  <image width="89" height="100" source="../图块-Kenney/6/PNG/Towers (red)/tower_43.png"/>
 </tile>
 <tile id="218">
  <image width="79" height="71" source="../图块-Kenney/6/PNG/Towers (red)/tower_44.png"/>
 </tile>
 <tile id="219">
  <image width="79" height="71" source="../图块-Kenney/6/PNG/Towers (red)/tower_45.png"/>
 </tile>
 <tile id="220">
  <image width="95" height="76" source="../图块-Kenney/6/PNG/Towers (red)/tower_46.png"/>
 </tile>
 <tile id="221">
  <image width="95" height="76" source="../图块-Kenney/6/PNG/Towers (red)/tower_47.png"/>
 </tile>
 <tile id="222">
  <image width="90" height="74" source="../图块-Kenney/6/PNG/Towers (red)/tower_48.png"/>
 </tile>
 <tile id="223">
  <image width="90" height="74" source="../图块-Kenney/6/PNG/Towers (red)/tower_49.png"/>
 </tile>
 <tile id="224">
  <image width="103" height="78" source="../图块-Kenney/6/PNG/Towers (red)/tower_50.png"/>
 </tile>
 <tile id="225">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_51.png"/>
 </tile>
 <tile id="226">
  <image width="103" height="78" source="../图块-Kenney/6/PNG/Towers (red)/tower_52.png"/>
 </tile>
 <tile id="227">
  <image width="93" height="75" source="../图块-Kenney/6/PNG/Towers (red)/tower_53.png"/>
 </tile>
 <tile id="228">
  <image width="79" height="72" source="../图块-Kenney/6/PNG/Towers (red)/tower_54.png"/>
 </tile>
</tileset>

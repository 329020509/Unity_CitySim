﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Client : MonoBehaviour {
    public GameObject UILayer;
    public GameObject GameLayer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StartGame()
    {
        UILayer.transform.Find("UICanvas/StartView").gameObject.SetActive(false);
        GameLayer.transform.Find("Parallax").gameObject.SetActive(false);
        gameObject.AddComponent<ScreenTouhcer>();
    }
}

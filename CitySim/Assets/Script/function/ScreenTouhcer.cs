﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTouhcer : MonoBehaviour {

    private Vector3 m_PositionBegan = Vector3.zero;
    private Vector3 m_PositionEnd = Vector3.zero;

    private Vector3 m_PositionCurrent = Vector3.zero;//触摸当前位置
    private Vector3 m_PositionPrevious = Vector3.zero;//上一位置

    private Vector3 moveVector = Vector3.zero;//平移向量

    private Transform terrain;//平移的地形;

    public int moveSpeed = 1;
    public struct InputPhase
    {
        public TouchPhase phase;
        public Vector3 position;
    }
	// Use this for initialization
	void Start () {
        terrain = GameObject.Find("terrain").GetComponent<Transform>();
    }
    
    private void LateUpdate()
    {
        //获知是鼠标还是触摸屏事件
        InputPhase inputPhase = GetInputPhase();

        if (inputPhase.position != null)
        {
            switch (inputPhase.phase)
            {
                case TouchPhase.Began:
                    TouchBegan(inputPhase.position);
                    moveVector = Vector3.zero;
                    m_PositionPrevious = Input.mousePosition;
                    break;
                case TouchPhase.Ended:
                    TouchEnd(inputPhase.position);
                    moveVector = Vector3.zero;
                    m_PositionPrevious = Vector3.zero;
                    m_PositionCurrent = Vector3.zero;
                    break;
                default:
                   moveVector = GetTouchCurrentDir(inputPhase.position);
                    break;
            }
        }
        if(m_PositionPrevious!=Vector3.zero)
        {
            terrain.Translate(moveSpeed * Time.fixedDeltaTime * moveVector);
        }
    }


    private void TouchBegan(Vector3 pos)
    {
        m_PositionBegan = pos;
        Debug.Log("触摸输入 位置:"+pos);
    }

    private void TouchEnd(Vector3 pos)
    {
        m_PositionEnd = pos;
        Debug.Log("触摸结束 位置:" + pos);
    }

    private Vector3 GetTouchCurrentDir(Vector3 pos)
    {
        m_PositionCurrent = pos;
        Vector3 dir = m_PositionCurrent - m_PositionPrevious;
        m_PositionPrevious = pos;
        Debug.Log("触摸持续 向量:" + dir);
        return dir;
       
    }


    private InputPhase GetInputPhase()
    {

        InputPhase inputPhase = new InputPhase();
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            inputPhase.phase = TouchPhase.Began;
            inputPhase.position = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            inputPhase.phase = TouchPhase.Ended;
            inputPhase.position = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0) && m_PositionPrevious!=Vector3.zero)
        {
            inputPhase.phase = TouchPhase.Moved;
            inputPhase.position = Input.mousePosition;
        }
        
#elif UNITY_IOS || UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            inputPhase.phase = touch.phase;
            inputPhase.position = touch.position;
        }
#else
        Debug.Log("未支持该平台触摸哦");
#endif

        return inputPhase;
    }


}
